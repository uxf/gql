<?php

declare(strict_types=1);

namespace UXF\GQL\Mapper\Parameter;

use BackedEnum;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Type\Definition\Type;
use LogicException;
use phpDocumentor\Reflection\DocBlock;
use phpDocumentor\Reflection\Type as ReflectionType;
use phpDocumentor\Reflection\Types\Array_;
use phpDocumentor\Reflection\Types\Compound;
use Ramsey\Uuid\UuidInterface;
use ReflectionNamedType;
use ReflectionParameter;
use TheCodingMachine\GraphQLite\Annotations\ParameterAnnotations;
use TheCodingMachine\GraphQLite\Mappers\Parameters\ParameterHandlerInterface;
use TheCodingMachine\GraphQLite\Mappers\Parameters\ParameterMiddlewareInterface;
use TheCodingMachine\GraphQLite\Parameters\ParameterInterface;
use UXF\Core\Attribute\Entity;
use UXF\Core\Utils\ReflectionHelper;
use UXF\GQL\Attribute\Entity as GQLEntity;
use UXF\GQL\Mapper\EnumTypeMapper;
use UXF\GQL\Mapper\UXFCoreTypeMapper;

final readonly class EntityParameterHandler implements ParameterMiddlewareInterface
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function mapParameter(ReflectionParameter $parameter, DocBlock $docBlock, ?ReflectionType $paramTagType, ParameterAnnotations $parameterAnnotations, ParameterHandlerInterface $next): ParameterInterface
    {
        $entity = $parameter->getAttributes(Entity::class)[0] ?? $parameter->getAttributes(GQLEntity::class)[0] ?? null;

        if ($entity !== null) {
            $type = $parameter->getType();
            if (!$type instanceof ReflectionNamedType) {
                throw new LogicException();
            }

            $isArray = $type->getName() === 'array';

            /** @var Entity $attribute */
            $attribute = $entity->newInstance();
            $propertyName = $attribute->property;

            if ($isArray) {
                if ($paramTagType instanceof Compound) {
                    $paramTagType = $paramTagType->get(0);
                }
                if (!$paramTagType instanceof Array_) {
                    throw new LogicException('Invalid $paramTagType type. ' . get_debug_type($paramTagType) . ' given.');
                }

                $typeName = $paramTagType->getValueType()->__toString();
            } else {
                $typeName = $type->getName();
            }

            if (!class_exists($typeName)) {
                throw new LogicException("$typeName is not class");
            }

            if (ReflectionHelper::findReflectionProperty($typeName, $propertyName) === null) {
                throw new LogicException("Invalid property $typeName::\$$propertyName");
            }

            $doctrineType = $this->inspectPropertyType($typeName, $propertyName);

            return $isArray
                ? new EntityArrayParameter(
                    entityManager: $this->entityManager,
                    className: $typeName,
                    name: $parameter->name,
                    nullable: $parameter->allowsNull(),
                    hasDefaultValue: $parameter->isDefaultValueAvailable(),
                    defaultValue: $parameter->isDefaultValueAvailable() ? $parameter->getDefaultValue() : null,
                    entityPropertyName: $propertyName,
                    entityPropertyType: $doctrineType,
                )
                : new EntityParameter(
                    entityManager: $this->entityManager,
                    className: $typeName,
                    name: $parameter->name,
                    nullable: $parameter->allowsNull(),
                    hasDefaultValue: $parameter->isDefaultValueAvailable(),
                    entityPropertyName: $propertyName,
                    entityPropertyType: $doctrineType,
                );
        }

        return $next->mapParameter($parameter, $docBlock, $paramTagType, $parameterAnnotations);
    }

    /**
     * @param class-string $typeName
     */
    private function inspectPropertyType(string $typeName, string $propertyName): ScalarType|EnumType
    {
        $metadata = $this->entityManager->getClassMetadata($typeName);

        $type = $metadata->reflFields[$propertyName]?->getType();
        assert($type instanceof ReflectionNamedType);

        $entityType = $type->getName();

        // object as id
        if (isset($metadata->associationMappings[$propertyName])) {
            if (!class_exists($entityType)) {
                throw new LogicException();
            }
            return $this->inspectPropertyType($entityType, 'id');
        }

        return match (true) {
            $entityType === 'int' => Type::int(),
            is_a($entityType, UuidInterface::class, true) => UXFCoreTypeMapper::getUuidType(),
            is_a($entityType, BackedEnum::class, true) => EnumTypeMapper::register($entityType),
            default => Type::string(),
        };
    }
}
