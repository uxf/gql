<?php

declare(strict_types=1);

namespace UXF\GQL\Mapper\Parameter;

use LogicException;
use phpDocumentor\Reflection\DocBlock;
use phpDocumentor\Reflection\Type;
use Psr\Container\ContainerInterface;
use ReflectionNamedType;
use ReflectionParameter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use TheCodingMachine\GraphQLite\Annotations\ParameterAnnotations;
use TheCodingMachine\GraphQLite\Mappers\Parameters\ParameterHandlerInterface;
use TheCodingMachine\GraphQLite\Mappers\Parameters\ParameterMiddlewareInterface;
use TheCodingMachine\GraphQLite\Parameters\ParameterInterface;
use UXF\GQL\Attribute\Inject;
use UXF\GQL\Service\Injector\InjectedArgument;

final readonly class InjectParameterHandler implements ParameterMiddlewareInterface
{
    /**
     * @param array<class-string, class-string> $map
     */
    public function __construct(
        private ContainerInterface $container,
        private RequestStack $requestStack,
        private array $map,
    ) {
    }

    public function mapParameter(ReflectionParameter $parameter, DocBlock $docBlock, ?Type $paramTagType, ParameterAnnotations $parameterAnnotations, ParameterHandlerInterface $next): ParameterInterface
    {
        $inject = $parameter->getAttributes(Inject::class)[0] ?? null;

        if ($inject !== null) {
            $type = $parameter->getType();
            if (!$type instanceof ReflectionNamedType) {
                throw new LogicException();
            }

            $service = $this->map[$type->getName()] ?? null;
            if ($service === null) {
                throw new LogicException();
            }

            return new InjectParameter(
                container: $this->container,
                service: $service,
                argument: new InjectedArgument($type->getName(), $type->allowsNull()),
                request: $this->requestStack->getMainRequest() ?? Request::create(''), // fake for cli
            );
        }

        return $next->mapParameter($parameter, $docBlock, $paramTagType, $parameterAnnotations);
    }
}
