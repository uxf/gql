<?php

declare(strict_types=1);

namespace UXF\GQL\Mapper;

use BackedEnum;
use TheCodingMachine\GraphQLite\Mappers\Root\RootTypeMapperFactoryContext;
use TheCodingMachine\GraphQLite\Mappers\Root\RootTypeMapperFactoryInterface;
use TheCodingMachine\GraphQLite\Mappers\Root\RootTypeMapperInterface;

final readonly class EnumTypeMapperFactory implements RootTypeMapperFactoryInterface
{
    /**
     * @param array<string, class-string<BackedEnum>> $typeMap - [gqlType => className]
     */
    public function __construct(private array $typeMap)
    {
    }

    public function create(RootTypeMapperInterface $next, RootTypeMapperFactoryContext $context): RootTypeMapperInterface
    {
        return new EnumTypeMapper($next, $this->typeMap);
    }
}
