<?php

declare(strict_types=1);

namespace UXF\GQL\Type;

use UXF\GraphQL\Type\Json as NewJson;

final readonly class Json extends NewJson
{
}
