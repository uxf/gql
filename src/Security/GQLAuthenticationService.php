<?php

declare(strict_types=1);

namespace UXF\GQL\Security;

use Symfony\Bundle\SecurityBundle\Security;
use TheCodingMachine\GraphQLite\Security\AuthenticationServiceInterface;

final readonly class GQLAuthenticationService implements AuthenticationServiceInterface
{
    public function __construct(private Security $security)
    {
    }

    public function isLogged(): bool
    {
        return $this->security->getUser() !== null;
    }

    public function getUser(): ?object
    {
        return $this->security->getUser();
    }
}
