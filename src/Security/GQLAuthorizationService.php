<?php

declare(strict_types=1);

namespace UXF\GQL\Security;

use Symfony\Bundle\SecurityBundle\Security;
use TheCodingMachine\GraphQLite\Security\AuthorizationServiceInterface;

final readonly class GQLAuthorizationService implements AuthorizationServiceInterface
{
    public function __construct(private Security $security)
    {
    }

    /**
     * @inheritDoc
     */
    public function isAllowed(string $right, $subject = null): bool
    {
        return $this->security->isGranted($right);
    }
}
