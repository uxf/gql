<?php

declare(strict_types=1);

namespace UXF\GQL\Service\Injector;

final readonly class InjectedArgument
{
    public function __construct(
        public string $type,
        public bool $nullable,
    ) {
    }
}
