<?php

declare(strict_types=1);

namespace UXF\GQL\Service;

use Closure;
use Symfony\Component\HttpFoundation\Response;

final class ResponseCallbackModifier
{
    /** @var array<Closure(Response $response): void> */
    private array $callbacks = [];

    /**
     * @param Closure(Response $callback): void $callback
     */
    public function add(Closure $callback): void
    {
        $this->callbacks[] = $callback;
    }

    /**
     * @internal
     */
    public function modify(Response $response): void
    {
        foreach ($this->callbacks as $callback) {
            $callback($response);
        }
    }
}
