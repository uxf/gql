<?php

declare(strict_types=1);

namespace UXF\GQL\Error;

use GraphQL\Error\Error;

interface ErrorHandler
{
    /**
     * @param Error[] $errors
     * @return mixed[]
     */
    public function __invoke(array $errors, callable $formatter): array;
}
