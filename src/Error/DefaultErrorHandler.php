<?php

declare(strict_types=1);

namespace UXF\GQL\Error;

use GraphQL\Error\CoercionError;
use GraphQL\Error\Error;
use GraphQL\Error\UserError;
use Psr\Log\LoggerInterface;
use TheCodingMachine\GraphQLite\Middlewares\MissingAuthorizationException;
use UXF\Core\Exception\CoreException;

final readonly class DefaultErrorHandler implements ErrorHandler
{
    public function __construct(
        private bool $debug,
        private LoggerInterface $logger,
    ) {
    }

    /**
     * @inheritDoc
     */
    public function __invoke(array $errors, callable $formatter): array
    {
        $errors = array_map([$this, 'process'], $errors);
        return array_map(static fn (Error $error) => $formatter($error), $errors);
    }

    private function process(Error $gqlError): Error
    {
        $this->logger->debug($gqlError, [
            'exception' => $gqlError,
        ]);

        $error = $gqlError->getPrevious() ?? $gqlError;

        // custom mapper input error
        if ($error instanceof UserError) {
            return new Error($error->getMessage(), extensions: [
                'code' => 'BAD_USER_INPUT',
                'debug' => $gqlError->getMessage(),
            ]);
        }

        // scalar input error
        if ($error instanceof CoercionError) {
            $error = $error->getPrevious() ?? $error;
            return new Error($error->getMessage(), extensions: [
                'code' => 'BAD_USER_INPUT',
                'debug' => $gqlError->getMessage(),
            ]);
        }

        if ($error instanceof CoreException) {
            $this->logger->log($error->getLevel(), $error->getMessage(), [
                'exception' => $error,
            ]);

            $extensions = [
                'code' => $error->getErrorCode(),
                'validationErrors' => $error->getValidationErrors(),
            ];

            if (in_array($error->getHttpStatusCode(), [401, 403], true)) {
                $extensions['category'] = 'security';
            }

            return new Error($error->getMessage(), extensions: $extensions);
        }

        if ($error instanceof MissingAuthorizationException) {
            return new Error($error->getMessage(), extensions: [
                'code' => match ($error->getCode()) {
                    401 => 'UNAUTHORIZED',
                    403 => 'FORBIDDEN',
                    default => 'SERVER_ERROR',
                },
                'category' => 'security',
            ]);
        }

        if (!$gqlError->isClientSafe()) {
            $this->logger->critical($error->getMessage(), [
                'exception' => $error,
            ]);

            return $this->debug ? throw $error : new Error('SERVER ERROR', extensions: [
                'code' => 'SERVER_ERROR',
            ]);
        }

        return $gqlError;
    }
}
