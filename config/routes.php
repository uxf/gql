<?php

declare(strict_types=1);

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\GQL\Controller\GQLController;

return static function (RoutingConfigurator $routingConfigurator): void {
    $routingConfigurator->add('uxf_graphql', '/graphql')
        ->controller(GQLController::class)
        ->methods(['GET', 'POST']);
};
