<?php

declare(strict_types=1);

use Ramsey\Uuid\Doctrine\UuidType;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Core\Test\Client;
use UXF\GQLTests\DataFixtures\DonaldFixtures;
use UXF\GQLTests\Project\MickeyService;
use UXF\GQLTests\Project\Staff\Coyote;
use UXF\GQLTests\Project\Staff\CoyoteResolver;
use UXF\GQLTests\Project\TestAuthenticator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure();

    $services->set(Client::class);
    $services->alias('test.client', Client::class)->public();

    $services->set(DonaldFixtures::class);

    $containerConfigurator->extension('framework', [
        'test' => true,
        'validation' => [
            'email_validation_mode' => 'html5',
        ],
        'http_method_override' => false,
    ]);

    $containerConfigurator->extension('security', [
        'providers' => [
            'in_memory' => [
                'memory' => [
                    'users' => [
                        'testUser' => [
                            'password' => 'pass',
                            'roles' => ['ROLE_USER'],
                        ],
                    ],
                ],
            ],
        ],
        'firewalls' => [
            'main' => [
                'custom_authenticators' => [
                    TestAuthenticator::class,
                ],
            ],
        ],
    ]);

    $containerConfigurator->extension('doctrine', [
        'dbal' => [
            'url' => 'sqlite:///%kernel.project_dir%/var/db.sqlite',
            'types' => [
                'uuid' => UuidType::class,
            ],
        ],
        'orm' => [
            'naming_strategy' => 'doctrine.orm.naming_strategy.underscore_number_aware',
            'auto_mapping' => true,
            'report_fields_where_declared' => true,
            'enable_lazy_ghost_objects' => true,
            'mappings' => [
                'test' => [
                    'type' => 'attribute',
                    'dir' => __DIR__ . '/../Project/Entity',
                    'prefix' => 'UXF\GQLTests\Project\Entity',
                ],
            ],
        ],
    ]);

    $containerConfigurator->extension('uxf_gql', [
        'destination' => __DIR__ . '/../generated/schema.graphql',
        'sources' => [
            __DIR__ . '/../../tests/Project',
        ],
        'injected' => [
            Coyote::class => CoyoteResolver::class,
        ],
        'allow_money' => true,
    ]);

    $services->set(TestAuthenticator::class);
    $services->set(MickeyService::class);
    $services->set(CoyoteResolver::class);
};
