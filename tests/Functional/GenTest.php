<?php

declare(strict_types=1);

namespace UXF\GQLTests\Functional;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class GenTest extends KernelTestCase
{
    public function test(): void
    {
        $kernel = self::bootKernel();

        $app = new Application($kernel);
        $cmd = $app->find('uxf:gql-gen');
        $tester = new CommandTester($cmd);
        $tester->execute([]);
        $tester->assertCommandIsSuccessful();

        self::assertFileEquals(__DIR__ . '/expected/schema.graphql', __DIR__ . '/../generated/schema.graphql');
    }
}
