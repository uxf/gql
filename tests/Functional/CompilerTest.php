<?php

declare(strict_types=1);

namespace UXF\GQLTests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use UXF\GQLTests\Project\Mutation\GoofyMutation;
use UXF\GQLTests\Project\Mutation\LoginMutation;
use UXF\GQLTests\Project\Mutation\PlutoMutation;
use UXF\GQLTests\Project\Query\DaysiQuery;
use UXF\GQLTests\Project\Query\DonaldQuery;
use UXF\GQLTests\Project\Query\GoofyQuery;
use UXF\GQLTests\Project\Query\MickeyQuery;
use UXF\GQLTests\Project\Query\PlutoQuery;

class CompilerTest extends KernelTestCase
{
    public function test(): void
    {
        $expected = [
            GoofyMutation::class,
            LoginMutation::class,
            PlutoMutation::class,
            DaysiQuery::class,
            DonaldQuery::class,
            GoofyQuery::class,
            MickeyQuery::class,
            PlutoQuery::class,
        ];

        $names = self::getContainer()->getParameter('uxf_gql.controller_names');
        self::assertSame($expected, $names);
    }
}
