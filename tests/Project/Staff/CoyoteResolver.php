<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Staff;

use Symfony\Component\HttpFoundation\Request;
use UXF\GQL\Service\Injector\InjectedArgument;

class CoyoteResolver
{
    public function __invoke(Request $request, InjectedArgument $argument): Coyote
    {
        assert($argument->type === Coyote::class);
        return new Coyote();
    }
}
