<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Type;

enum GoofyEnum: int
{
    case FIRST = 1;
    case SECOND = 2;
}
