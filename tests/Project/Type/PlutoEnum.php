<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Type;

enum PlutoEnum: string
{
    case OK_WOW = 'OK';
    case ERROR = 'ERROR';
}
