<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Query;

use TheCodingMachine\GraphQLite\Annotations\Query;
use UXF\Core\Attribute\Entity;
use UXF\GQLTests\Project\Entity\Donald as AppDonald;
use UXF\GQLTests\Project\Type\Donald;

class MickeyQuery
{
    /**
     * @param AppDonald[] $donalds
     * @param AppDonald[]|null $donaldsNullable
     * @param AppDonald[]|null $donaldsNullableOptional
     * @param AppDonald[] $donaldsOptional
     */
    #[Query(name: 'mickey')]
    public function __invoke(
        #[Entity('uuid')] AppDonald $donald,
        #[Entity('minnie')] AppDonald $donaldX,
        #[Entity('enum')] AppDonald $donaldY,
        #[Entity('enumInt')] AppDonald $donaldZ,
        #[Entity('name')] array $donalds,
        #[Entity('name')] ?AppDonald $donaldNullable,
        #[Entity('uuid')] ?array $donaldsNullable,
        #[Entity('name')] ?AppDonald $donaldNullableOptional = null,
        #[Entity('name')] ?array $donaldsNullableOptional = null,
        #[Entity('name')] array $donaldsOptional = [],
    ): Donald {
        return new Donald($donald);
    }
}
