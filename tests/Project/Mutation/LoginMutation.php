<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Mutation;

use Symfony\Component\HttpFoundation\Response;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use UXF\GQL\Service\ResponseCallbackModifier;

final readonly class LoginMutation
{
    public function __construct(private ResponseCallbackModifier $responseModifier)
    {
    }

    #[Mutation(name: 'login')]
    public function __invoke(string $username, string $password): bool
    {
        $this->responseModifier->add(fn (Response $response) => $response->headers->set('Auth-Token', '1'));
        return true;
    }
}
