<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Mutation;

use Symfony\Component\Security\Core\User\InMemoryUser;
use TheCodingMachine\GraphQLite\Annotations\InjectUser;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use UXF\GQLTests\Project\Input\PlutoInput;
use UXF\GQLTests\Project\Type\Pluto;

class PlutoMutation
{
    #[Logged]
    #[Mutation(name: 'pluto')]
    public function __invoke(#[InjectUser] InMemoryUser $user, PlutoInput $pluto): Pluto
    {
        return new Pluto(
            string: $pluto->string,
            array: $pluto->array,
            date: $pluto->date,
            dateTime: $pluto->dateTime,
            time: $pluto->time,
            phone: $pluto->phone,
            email: $pluto->email,
            ninCze: $pluto->ninCze,
            banCze: $pluto->banCze,
            uuid: $pluto->uuid,
            money: $pluto->money,
            decimal: $pluto->decimal,
            url: $pluto->url,
            enum: $pluto->enum,
            enumInt: $pluto->enumInt,
            json: $pluto->json,
            long: $pluto->long,
        );
    }
}
